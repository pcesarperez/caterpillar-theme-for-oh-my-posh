<#
    Caterpillar.psm1

    Customized and modular theme for the `oh-my-posh` PowerShell module.
#>


#requires -Version 2 -Modules posh-git


<#
    .synopsis
    Sets the desired `oh-my-posh` theme.

    .description
    The user name in the current session will be part of the prompt.

    .parameter lastCommandFailed
    `true` if the last issued command failed or `false` otherwise.

    .parameter with
    Additional information to show in the prompt.

    .inputs
    None. You cannot pipe objects to Get-PromptUserNameFragment.

    .outputs
    The custom-tailored prompt.
#>
function Write-Theme ([bool] $lastCommandFailed, [string] $with) {
    $userName = Get-PromptUserNameFragment
    $machine = Get-PromptMachineFragment
    $currentPath = Get-PromptCurrentPathFragment
    $gitStatus = Get-PromptGitStatus


    return Get-FormattedPrompt -UserName $userName -Machine $machine -CurrentPath $currentPath -GitStatus $gitStatus -Failure $lastCommandFailed
}


<#
    .synopsis
    Gets a formatted prompt with the given elements.

    .description
    This is where the different fragments of the prompt get combined in a fancy way.

    .parameter UserName
    User name in the current session.

    .parameter Machine
    Machine name.

    .parameter CurrentPath
    Current path.

    .parameter GitStatus
    Git status in the current path.

    .parameter Failure
    Points out if the last command failed.

    .inputs
    None. You cannot pipe objects to Get-PromptUserNameFragment.

    .outputs
    Returns the formatted prompt.
#>
function Get-FormattedPrompt ($UserName, $Machine, $CurrentPath, $GitStatus, $Failure) {
    # Prompt colors.
    $identityForeground = [ConsoleColor]::Black
    $identityBackground = [ConsoleColor]::Blue
    $pathForeground = [ConsoleColor]::Black
    $pathBackground = [ConsoleColor]::DarkBlue
    $gitStatusForeground = [ConsoleColor]::Black
    $promptForeground = [ConsoleColor]::Green
    $failedCommandForeground = [ConsoleColor]::Yellow
    $failedCommandBackground = [ConsoleColor]::DarkRed

    # Prompt symbols.
    # You need to have Cascadia Code PL installed to properly see the symbols in the comments.
    $segmentSeparator = [char]::ConvertFromUtf32(0xE0B1) # 
    $sectionSeparator = [char]::ConvertFromUtf32(0xE0B0) # 
    $pathSeparator = [char]::ConvertFromUtf32(0xE0BB) # 
    $promptIndicator = [char]::ConvertFromUtf32(0x03BB) # λ
    $promptIndicatorAdmin = [char]::ConvertFromUtf32(0x222B) # ∫
    $failureIndicator = [char]::ConvertFromUtf32(0x2573) # ╳

    # Breaks the path into folder-size chunks.
    $mincedPath = Get-MincedPath -Path $CurrentPath

    # Breathing space before the actual prompt.
    $prompt = Set-Newline

    # Checks the last command state and indicate if it failed.
    if ($lastCommandFailed) {
        $prompt += Write-Prompt -Object " $failureIndicator " -ForegroundColor $failedCommandForeground -BackgroundColor $failedCommandBackground
        $prompt += Write-Prompt -Object $sectionSeparator -ForegroundColor $failedCommandBackground -BackgroundColor $identityBackground
    }

    # User name segment within identity section.
    $prompt += Write-Prompt -Object " $UserName " -ForegroundColor $identityForeground -BackgroundColor $identityBackground
    $prompt += Write-Prompt -Object "$segmentSeparator " -ForegroundColor $identityForeground -BackgroundColor $identityBackground

    # Machine name segment within identity section.
    $prompt += Write-Prompt -Object "$Machine " -ForegroundColor $identityForeground -BackgroundColor $identityBackground
    $prompt += Write-Prompt -Object "$sectionSeparator " -ForegroundColor $identityBackground -BackgroundColor $pathBackground

    # Current path section.
    for ($i = 0; $i -lt $mincedPath.length - 1; $i++) {
        $prompt += Write-Prompt -Object "$($mincedPath[$i]) " -ForegroundColor $pathForeground -BackgroundColor $pathBackground
        $prompt += Write-Prompt -Object "$pathSeparator " -ForegroundColor Black -BackgroundColor $pathBackground
    }
    $prompt += Write-Prompt -Object $mincedPath[$mincedPath.length - 1] -ForegroundColor $pathForeground -BackgroundColor $pathBackground
    $prompt += Write-Prompt -Object " " -ForegroundColor $pathForeground -BackgroundColor $pathBackground

    # Git status section.
    if ($GitStatus) {
        $prompt += Write-Prompt -Object $sectionSeparator -ForegroundColor $pathBackground -BackgroundColor $GitStatus.BackgroundColor
        $prompt += Write-Prompt -Object " $($GitStatus.VcInfo) " -ForegroundColor $gitStatusForeground -BackgroundColor $GitStatus.BackgroundColor
        $prompt += Write-Prompt -Object $sectionSeparator -ForegroundColor $GitStatus.BackgroundColor
    } else {
        $prompt += Write-Prompt -Object $sectionSeparator -ForegroundColor $pathBackground
    }

    # Prompt indicator.
    $prompt += Set-Newline
    if (Test-Administrator) {
        $prompt += Write-Prompt -Object $promptIndicatorAdmin -ForegroundColor $promptForeground
    } else {
        $prompt += Write-Prompt -Object $promptIndicator -ForegroundColor $promptForeground
    }
    $prompt += " "

    return $prompt
}


<#
    .synopsis
    Gets the user name in the current session.

    .description
    The user name in the current session will be part of the prompt.

    .inputs
    None. You cannot pipe objects to Get-PromptUserNameFragment.

    .outputs
    Returns the user name in the current session.
#>
function Get-PromptUserNameFragment {
    return $env:USERNAME
}


<#
    .synopsis
    Gets the machine name.

    .description
    The machine name will be part of the prompt.

    .inputs
    None. You cannot pipe objects to Get-PromptUserNameFragment.

    .outputs
    Returns the machine name.
#>
function Get-PromptMachineFragment {
    return $env:COMPUTERNAME
}


<#
    .synopsis
    Gets the current path.

    .description
    The current path will be part of the prompt.

    .inputs
    None. You cannot pipe objects to Get-PromptUserNameFragment.

    .outputs
    Returns the current path.
#>
function Get-PromptCurrentPathFragment {
    return Get-FullPath -dir $pwd
}


<#
    .synopsis
    Gets the Git status in the current path.

    .description
    The Git status in the current path will be part of the prompt.

    .inputs
    None. You cannot pipe objects to Get-PromptUserNameFragment.

    .outputs
    Returns the Git status in the current path.
#>
function Get-PromptGitStatus {
    $status = Get-VCSStatus

    if ($status) {
        return (Get-VcsInfo -status $status)
    } else {
        return ""
    }
}


<#
    .synopsis
    Gets the elements of a given path.

    .description
    Splits a given path into its elements.

    .parameter Path
    Path to split.

    .inputs
    None. You cannot pipe objects to Get-PromptUserNameFragment.

    .outputs
    Returns the elements of the path.
#>
function Get-MincedPath ($Path) {
    if ($Path -match "^[A-Z]:$") {
        return (, @($Path))
    } else {
        return $Path -split "\\"
    }
}